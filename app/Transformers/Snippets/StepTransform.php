<?php

namespace App\Transformers\Snippets;

use League\Fractal\TransformerAbstract;
use App\Models\Step;

class StepTransform extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
   
    public function transform(Step $step)
    {
        return [
            'uuid' => $step->uuid,
            'order' => (float) $step->order,
            'title' => $step->title ?: '',
            'body' => $step->body ?: ''
        ];
    }

}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Cart\Cart;
use Stripe\Stripe;
use App\Cart\Payments\Gateway;
use App\Cart\Payments\Gateways\StripeGateway;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if(config('app.env') === 'production') {
        \URL::forceScheme('https');
       }
        //Stripe::setApiKey(config('services.stripe.secret'));
    }
}

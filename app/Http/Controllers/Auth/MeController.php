<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PrivateUserResource;
use App\Transformers\Users\UserTransform;

class MeController extends Controller
{
	public function __construct()
	{
		$this->middleware(['auth:api']);
	}
    public function action(Request $request)
    {
    	 return fractal()
              ->item($request->user())
              ->transformWith(new UserTransform())
              ->toArray();
    }
}

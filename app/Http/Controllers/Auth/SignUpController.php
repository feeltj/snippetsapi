<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\Users\UserTransform;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class SignUpController extends Controller
{
    public function __invoke(Request $request)
    {
    	$this->validate($request, [
           'email' => 'required|email|unique:users,email',
           'username' => 'required|alpha_dash|unique:users,username',
           'name' =>'required',
           'password' =>'required|min:6|confirmed',
           'password_confirmation' =>'required',
    	]);

    	$user = User::create($request->only('email', 'name', 'username', 'password'));

    	return fractal()
    	       ->item($user)
    	       ->transformWith(new UserTransform())
    	       ->toArray();
    }
}

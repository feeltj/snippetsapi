<?php

namespace App\Http\Controllers\Me;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Snippet;
use App\Transformers\Snippets\SnippetTransform;

class SnippetController extends Controller
{
	public function __construct()
	{
       $this->middleware(['auth:api']);
	}

    public function index(Request $request)
    {
        return fractal()
               ->collection($request->user()->snippets)
               ->transformWith(new SnippetTransform()) 
               ->toArray()
               ;
    }
}

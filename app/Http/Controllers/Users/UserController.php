<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Transformers\Users\PublicUserTransform;

class UserController extends Controller
{
    public function show(User $user)
    {
        return fractal()
               ->item($user)
               ->transformWith(new PublicUserTransform())->toArray();
    }

    public function update(User $user, Request $request)
    {
    	$this->authorize('as', $user);
    	
    	$this->validate($request, [
           'email' => 'required|email|unique:users,email,' . $request->user()->id,
           'username' => 'required|alpha_dash|unique:users,username,' . $request->user()->id,
           'name' =>'required',
           'password' =>'nullable|min:6',
    	]);

    	$user->update(
           $request->only('email', 'name', 'username', 'password')
    	);
    }
}

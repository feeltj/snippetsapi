<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Transformers\Snippets\SnippetTransform;

class SnippetController extends Controller
{
    public function index(User $user,Request $request)
    {
        return fractal()
               ->collection(
                  $user->snippets()->public()->get()
               )
               ->transformWith(new SnippetTransform()) 
               ->toArray()
               ;
    }
}

<?php

namespace App\Http\Controllers\Snippets;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\Snippet;
use App\Transformers\Snippets\SnippetTransform;

class SnippetController extends Controller
{
	public function __construct()
	{
       $this->middleware(['auth:api'])
                        ->only('store', 'update');
	}

    public function index(Request $request)
    {
        return fractal()
               ->collection(
                  Snippet::take($request->get('limit', 20))->latest()->public()->get()
               )
               ->transformWith(new SnippetTransform()) 
               ->parseIncludes([
                    'author',
               ])
               ->toArray()
               ;
    }

	public function show(Snippet $snippet)
	{
		$this->authorize('show', $snippet);
		return fractal()
		        ->item($snippet)
		        ->transformWith(new SnippetTransform())
		        ->parseIncludes([
                    'steps',
                    'author',
                    'user'
		         ])
		        ->toArray();
	}

    public function store(Request $request)
    {
    	$snippet = $request->user()->snippets()->create();

    	return fractal()
		        ->item($snippet)
		        ->transformWith(new SnippetTransform())
		        ->toArray();
    }

    public function update(Snippet $snippet, Request $request)
    {
    	$this->authorize('update', $snippet);
    	$this->validate($request, [
            'title' => 'nullable',
            'is_public' => 'nullable|boolean'
    	]);

    	$snippet->update($request->only('title', 'is_public'));
    }

    public function destroy(Snippet $snippet, Request $request)
    {
        $this->authorize('destroy', $snippet);
        $snippet->delete();
    }
}

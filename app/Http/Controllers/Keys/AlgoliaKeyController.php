<?php

namespace App\Http\Controllers\Keys;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Algolia\AlgoliaSearch\SearchClient;

class AlgoliaKeyController extends Controller
{
    public function __invoke(Request $request)
    {
    	return response()->json([
           'data' => SearchClient::generateSecuredApiKey(
              config('scout.algolia.key'), [
                  'filters' => 'data.is_public:true'
              ]
             )
    	]);
    }
}

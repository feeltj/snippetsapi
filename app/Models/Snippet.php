<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Snippet;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Scout\Searchable;
use App\Transformers\Snippets\SnippetTransform;


class Snippet extends Model
{
	//use Searchable;

    protected $fillable = [
       'uuid',
       'title',
       'is_public'
    ];

    public function getRouteKeyName()
    {
    	return 'uuid';
    }

    public function toSearchableArray()
    {
    	return fractal()
    	       ->item($this)
    	       ->transformWith(new SnippetTransform())
    	       ->parseIncludes([
                 'author',
                 'steps'
    	       ])
    	       ->toArray();
    }

    public static function boot()
    {
    	parent::boot();

    	static::created(function (Snippet $snippet){
             $snippet->steps()->create([
               'order' => 1
             ]);
    	});

    	static::creating(function (Snippet $snippet){
             $snippet->uuid = Str::uuid();
    	});
    }

    public function isPublic() 
    {
        return $this->is_public;
    }

    public function scopePublic(Builder $builder)
    {
        return $builder->where('is_public', true);
    }

    public function steps()
    {
    	return $this->hasMany(Step::class)->orderBy('order', 'asc');
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
